package com.piura.rutaspiura.util;

public class Constantes {

    private Constantes() {
    }

    public static final String URL_SERVICES="http://35.225.248.191:8085/";
    public static final String URL_API_GOOGLE = "https://maps.googleapis.com/maps/";

    public static final String ERROR = "ERROR :";

}
