package com.piura.rutaspiura.adapter;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.piura.rutaspiura.R;
import com.piura.rutaspiura.models.Ruta;
import com.piura.rutaspiura.util.Utilitario;

public class MarkerClusterRenderer extends DefaultClusterRenderer<Ruta> implements ClusterManager.OnClusterClickListener<Ruta>, GoogleMap.OnInfoWindowClickListener {

    private GoogleMap googleMap;
    private LayoutInflater layoutInflater;
    private final IconGenerator clusterIconGenerator;
    private final View clusterItemView;
    private final Context context;

    public MarkerClusterRenderer(@NonNull Context context, GoogleMap map, ClusterManager<Ruta> clusterManager) {
        super(context, map, clusterManager);
        this.context = context;
        this.googleMap = map;

        layoutInflater = LayoutInflater.from(context);

        clusterItemView = layoutInflater.inflate(R.layout.single_cluster_marker_view, null);

        clusterIconGenerator = new IconGenerator(context);
        Drawable drawable = ContextCompat.getDrawable(context, android.R.color.transparent);
        clusterIconGenerator.setBackground(drawable);
        clusterIconGenerator.setContentView(clusterItemView);

        clusterManager.setOnClusterClickListener(this);

        googleMap.setInfoWindowAdapter(clusterManager.getMarkerManager());

        googleMap.setOnInfoWindowClickListener(this);

        clusterManager.getMarkerCollection().setOnInfoWindowAdapter(new MyCustomClusterItemInfoView(layoutInflater));

        googleMap.setOnCameraIdleListener(clusterManager);

        googleMap.setOnMarkerClickListener(clusterManager);
    }

    @Override
    protected void onBeforeClusterItemRendered(Ruta item, MarkerOptions markerOptions) {
        if ("RUTA U6 - SUPER STAR".equals(item.getNombre())) {
            markerOptions.icon(Utilitario.bitmapDescriptorFromVector(context, R.drawable.ic_station_rojo));
            markerOptions.title(item.getParada());
        } else if ("RUTA U9 - GUADALUPE".equals(item.getNombre())) {
            markerOptions.icon(Utilitario.bitmapDescriptorFromVector(context, R.drawable.ic_station_amarilla));
            markerOptions.title(item.getParada());
        } else if ("RUTA U4 - SOL DE PIURA".equals(item.getNombre())){
            markerOptions.icon(Utilitario.bitmapDescriptorFromVector(context, R.drawable.ic_station_azul));
            markerOptions.title(item.getParada());
        } else if ("RUTA U8 - EMUTSA".equals(item.getNombre())){
            markerOptions.icon(Utilitario.bitmapDescriptorFromVector(context, R.drawable.ic_station_verde));
            markerOptions.title(item.getParada());
        } else if ("RUTA I13 - TRANS PIURA".equals(item.getNombre())){
            markerOptions.icon(Utilitario.bitmapDescriptorFromVector(context, R.drawable.ic_station_celeste));
            markerOptions.title(item.getParada());
        }
    }

    @Override
    protected void onBeforeClusterRendered(Cluster<Ruta> cluster, MarkerOptions markerOptions) {
        TextView singleClusterMarkerSizeTextView = clusterItemView.findViewById(R.id.singleClusterMarkerSizeTextView);
        singleClusterMarkerSizeTextView.setText(String.valueOf(cluster.getSize()));
        Bitmap icon = clusterIconGenerator.makeIcon();
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
    }

    @Override
    protected void onClusterItemRendered(Ruta clusterItem, Marker marker) {
        marker.setTag(clusterItem);
    }

    @Override
    public boolean onClusterClick(Cluster<Ruta> cluster) {
        if (cluster == null) return false;
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Ruta ruta : cluster.getItems())
            builder.include(new LatLng(ruta.getLatitud(), ruta.getLongitud()));
        LatLngBounds bounds = builder.build();
        try {
            googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Context context = clusterItemView.getContext();
        Ruta ruta = (Ruta) marker.getTag(); //  handle the clicked marker object
        if (context != null && ruta != null) {
/*
                if(ruta.getFlagTipoRuta()>0){
                        for (int i = 0; i <ruta.getNumeroParada() ; i++) {
                            if("I".equals(ruta.getTipoRuta())){
                                if(i==0) {
                                    obtenerRutasWs(latitudInicial, longitudInicial,
                                            getItems().get(i).getPosition().latitude, getItems().get(i).getPosition().longitude,
                                            getItems().get(i).getColor());
                                }else{
                                    obtenerRutasWs(getItems().get(i-1).getPosition().latitude, getItems().get(i-1).getPosition().longitude,
                                            getItems().get(i).getPosition().latitude, getItems().get(i).getPosition().longitude, getItems().get(i).getColor());
                                }
                            }else{

                            }

                        }
                }else{
                    Toast.makeText(context, ruta.getTitle(), Toast.LENGTH_SHORT).show();
                }
                Toast.makeText(context, ruta.getRuta(), Toast.LENGTH_SHORT).show();
            }*/
        }


    }


}