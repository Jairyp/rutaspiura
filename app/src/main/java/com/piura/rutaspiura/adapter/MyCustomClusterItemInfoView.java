package com.piura.rutaspiura.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.piura.rutaspiura.R;
import com.piura.rutaspiura.models.Ruta;

 public class MyCustomClusterItemInfoView implements GoogleMap.InfoWindowAdapter {

    private final View clusterItemView;

    public MyCustomClusterItemInfoView(LayoutInflater layoutInflater) {
        clusterItemView = layoutInflater.inflate(R.layout.marker_info_window, null);
    }

    @Override
    public View getInfoWindow(Marker marker) {
        Ruta ruta = (Ruta) marker.getTag();
        if (ruta == null) return clusterItemView;
        TextView itemTransporte = clusterItemView.findViewById(R.id.itemTransporte);
        TextView itemRuta = clusterItemView.findViewById(R.id.itemRuta);
        itemTransporte.setText(marker.getTitle());
        itemRuta.setText(ruta.getNombre());
        return clusterItemView;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }
}
