package com.piura.rutaspiura.services;


import com.piura.rutaspiura.models.webDto.RutasWebDto;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface RutaServices {

    @GET("/rutas")
    Call<RutasWebDto> getList();

    @GET("/rutas/{idRuta}")
    Call<RutasWebDto> getByCodeRuta(@Path("idRuta") int idRuta);

}
