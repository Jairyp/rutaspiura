package com.piura.rutaspiura.services;

import com.piura.rutaspiura.models.DirectionResults;
import com.piura.rutaspiura.util.Constantes;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiGoogleServiceImpl implements ApiGoogleService{

    private ApiGoogleService apiGoogleService;

    public ApiGoogleServiceImpl() {
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(Constantes.URL_API_GOOGLE)
                .build();
        apiGoogleService = retrofit.create(ApiGoogleService.class);
    }

    @Override
    public Call<DirectionResults> getDirecction(String origin, String destination) {
        return apiGoogleService.getDirecction(origin,destination);
    }
}
