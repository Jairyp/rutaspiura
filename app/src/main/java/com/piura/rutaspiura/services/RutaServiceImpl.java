package com.piura.rutaspiura.services;

import com.piura.rutaspiura.models.Ruta;
import com.piura.rutaspiura.models.webDto.RutasWebDto;
import com.piura.rutaspiura.util.Constantes;


import java.util.List;

import retrofit2.Call;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RutaServiceImpl implements RutaServices{

    private RutaServices rutaServices;


    public RutaServiceImpl() {

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(Constantes.URL_SERVICES)
                .build();
        rutaServices = retrofit.create(RutaServices.class);

    }


    @Override
    public Call<RutasWebDto> getList() {
        return rutaServices.getList();
    }

    @Override
    public Call<RutasWebDto> getByCodeRuta(int idRuta) {
        return rutaServices.getByCodeRuta(idRuta);
    }
}
