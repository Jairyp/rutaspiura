package com.piura.rutaspiura.services;

import com.piura.rutaspiura.models.DirectionResults;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiGoogleService {


    @GET("api/directions/json?key=AIzaSyDER9WsLkDjJqjuFr1cr3IEaJuLmVQJVNE&callback=initialize")
    Call<DirectionResults> getDirecction(@Query("origin") String origin, @Query("destination") String destination);

}
