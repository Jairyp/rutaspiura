package com.piura.rutaspiura.activitys;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.piura.rutaspiura.R;
import com.piura.rutaspiura.adapter.MarkerClusterRenderer;
import com.piura.rutaspiura.models.DirectionResults;
import com.piura.rutaspiura.models.Ruta;
import com.piura.rutaspiura.models.webDto.RutasWebDto;
import com.piura.rutaspiura.services.ApiGoogleServiceImpl;
import com.piura.rutaspiura.services.RutaServiceImpl;
import com.piura.rutaspiura.util.Constantes;
import com.piura.rutaspiura.util.Utilitario;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        AdapterView.OnItemClickListener,
        LocationListener{

    private GoogleMap mMap;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;

    private RutaServiceImpl rutaService;
    private RutasWebDto rutasWeb;
    private Call<RutasWebDto> call;

    private ApiGoogleServiceImpl apiGoogleService;
    private DirectionResults directionResults;
    private Call<DirectionResults> callDirecction;

    private AutoCompleteTextView autoCompleteTextView;
    private Button myLocationButton;
    private ArrayList permissionsToRequest;
    private ArrayList permissions = new ArrayList<>();
    private final static int ALL_PERMISSIONS_RESULT = 101;

    int INTERVAL = 1000;
    int FASTEST_INTERVAL = 500;
    private Double longitude;
    private Double latitude;
    private AlertDialog.Builder alertUbicacion;
    private AlertDialog alert = null;

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    static final int LOCATION_SETTINGS_REQUEST = 1;
    private Location mLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        permissionsToRequest = findUnAskedPermissions(permissions);
        alertUbicacion = new AlertDialog.Builder(this);

        if (!hasGooglePlayServices()) {
            Toast.makeText(this, "Google play services no encontrado.", Toast.LENGTH_LONG).show();
            return;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(!permissionsToRequest.isEmpty()){
            requestPermissions((String[]) permissionsToRequest.toArray(
                    new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
            }
        }

        String[] rutas = getResources().getStringArray(R.array.rutas);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.custom_list_item, R.id.text_view_list_item, rutas);
        autoCompleteTextView = findViewById(R.id.actv);
        autoCompleteTextView.setAdapter(adapter);
        autoCompleteTextView.setOnItemClickListener(this);

        myLocationButton = findViewById(R.id.my_gps_location);
        myLocationButton.setOnClickListener(v -> getCurrentLocation());

        mLocationRequest = new LocationRequest();
        mLocationRequest.setNumUpdates(1);
        mLocationRequest.setExpirationTime(5000);
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        //Obtenga el SupportMapFragment y reciba una notificación cuando el mapa esté listo para ser utilizado.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        //Instanciamos los servicios rest y el api de google
        rutaService = new RutaServiceImpl();
        apiGoogleService = new ApiGoogleServiceImpl();

        checkPermissions();

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                mMap.setMyLocationEnabled(false);
            } else {
                //Preguntamos si tiene Permisos GPS
                checkLocationPermission();
            }
        }else {
            mMap.setMyLocationEnabled(false);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case 100: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setUpGoogleApiClient();
                } else {
                    Toast.makeText(this, "Permission Denegado!", Toast.LENGTH_SHORT).show();
                }
            }
            break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOCATION_SETTINGS_REQUEST) {
            // user is back from location settings - check if location services are now enabled
            checkGPSisOpen();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(1000); // Intervals millis
        mLocationRequest.setFastestInterval(500); //If avaible sooner

        if (!isPermissionGranted(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            return;
        }

        if (!checkGPSisOpen()) {
            Toast.makeText(this, "Activando el GPS.", Toast.LENGTH_SHORT)
                    .show();
            Intent viewIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(viewIntent);
        } else {
            LocationServices.FusedLocationApi
                    .requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            getCoords(LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        setUpGoogleApiClient();
    }

    @Override
    public void onLocationChanged(Location location) {
            getCoords(location);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        mMap.clear();
        CharSequence ruta = ((TextView) view.findViewById(R.id.text_view_list_item)).getText();
        AsyncTask task = new ProgressTask(this,ruta).execute();
    }

    private void checkPermissions() {
        if (isPermissionGranted(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            setUpGoogleApiClient();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 100);
        }
    }

    synchronized void setUpGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    public static boolean isPermissionGranted(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission) ==
                PackageManager.PERMISSION_GRANTED;
    }

    private void getCurrentLocation() {
        Location location = null;
        if (checkPermission()) {
            LocationServices.FusedLocationApi
                    .requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }

        if (location != null) {
            //Obteniendo locazacion actual
            longitude = location.getLongitude();
            latitude = location.getLatitude();

            //mover la pantalla a la locaizacion actual
            moveMap();
        }
    }

    private void moveMap() {
        //Limpiando map
        mMap.clear();

        //Creando coordenadas
        LatLng latLng = new LatLng(latitude, longitude);

        //Agregando Market
        mMap.addMarker(new MarkerOptions()
                .position(latLng) //Posicion
                .draggable(true) //para que el market se puede mover
                .title("Posicion Actual")); //Titulo del Marker

        //Moviendo camara
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        //Zoon de la camara
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
    }

    public boolean checkPermission() {

        int FirstPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        int SecondPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION);

        return FirstPermissionResult == PackageManager.PERMISSION_GRANTED &&
                SecondPermissionResult == PackageManager.PERMISSION_GRANTED;

    }

    private ArrayList findUnAskedPermissions(ArrayList<String> wanted) {
        //Buscar permisos no solicitados
        ArrayList result = new ArrayList();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        //Verifica si tiene permisos
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    private void cargarRutasGoogleApi(float latitudInicio,float longitudInicio,float latitudFin,float longitudFin,int color){

        callDirecction = apiGoogleService.getDirecction(String.valueOf(latitudInicio).concat(",".concat(String.valueOf(longitudInicio))),
                String.valueOf(latitudFin).concat(",".concat(String.valueOf(longitudFin))));
        callDirecction.enqueue(new Callback<DirectionResults>() {
            @Override
            public void onResponse(Call<DirectionResults> call, retrofit2.Response<DirectionResults> response) {
                directionResults = response.body();
                Utilitario.printRuta(directionResults, color,mMap);
            }

            @Override
            public void onFailure(Call<DirectionResults> call, Throwable t) {
                Log.d("ERROR: ", t.toString());
            }
        });

    }

    private void setUpClusterManager(GoogleMap googleMap,List<Ruta> lista) {
        ClusterManager<Ruta> clusterManager = new ClusterManager<>(this, googleMap);
        MarkerClusterRenderer markerClusterRenderer = new MarkerClusterRenderer(this, googleMap, clusterManager);
        clusterManager.setRenderer(markerClusterRenderer);
        clusterManager.addItems(lista);
        clusterManager.cluster();

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lista.get(0).getLatitud(),lista.get(0).getLongitud()),15));

    }

    private void mostrarDialogo(){
        new Thread() {
            public void run() {
                MapsActivity.this.runOnUiThread(new Runnable(){

                    @Override
                    public void run(){
                        alertUbicacion.setTitle("Importante");
                        alertUbicacion.setMessage("Debe seleccionar su ubicacion, desea continuar?");
                        alertUbicacion.setCancelable(false);
                        alertUbicacion.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogo1, int id) {
                                aceptar();
                            }
                        });
                        alertUbicacion.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogo1, int id) {
                                cancelar();
                            }
                        });
                        alertUbicacion.show();
                    }
                });
            }
        }.start();

    }

    private void aceptar(){
        Toast t=Toast.makeText(this,"Obteniendo Ubicacion", Toast.LENGTH_SHORT);
        limpiar();
        getCurrentLocation();
        t.show();
    }

    private void cancelar(){
        alertUbicacion.setOnDismissListener(DialogInterface::dismiss);
    }

    private void limpiar(){
        mMap.clear();
        autoCompleteTextView.clearListSelection();
        autoCompleteTextView.setText("");
    }

    private boolean hasGooglePlayServices() {
        return GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this) ==
                ConnectionResult.SUCCESS;
    }

    private boolean checkGPSisOpen() {
        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    public void cargarRutas(Integer idRuta){
        call = rutaService.getByCodeRuta(idRuta);
        call.enqueue(new Callback<RutasWebDto>() {
            @Override
            public void onResponse(Call<RutasWebDto> call, retrofit2.Response<RutasWebDto> response) {
                rutasWeb = response.body();
                for (int j = 0; j <rutasWeb.getRutas().size() ; j++) {
                    if(j==0) {
                        cargarRutasGoogleApi(latitude.floatValue(),longitude.floatValue(),rutasWeb.getRutas().get(j).getLatitud()
                                ,rutasWeb.getRutas().get(j).getLongitud(),Color.BLUE);

                    }else{
                        if(rutasWeb.getRutas().get(j).getFlagTipoRuta()==1) {
                            cargarRutasGoogleApi(rutasWeb.getRutas().get(j-1).getLatitud(), rutasWeb.getRutas().get(j-1).getLongitud(), rutasWeb.getRutas().get(j).getLatitud()
                                    , rutasWeb.getRutas().get(j).getLongitud(),Color.GREEN);

                        }else{
                            cargarRutasGoogleApi(rutasWeb.getRutas().get(j-1).getLatitud(), rutasWeb.getRutas().get(j-1).getLongitud(), rutasWeb.getRutas().get(j).getLatitud()
                                    , rutasWeb.getRutas().get(j).getLongitud(),Color.RED);

                        }
                    }
                }
                setUpClusterManager(mMap,rutasWeb.getRutas());
            }

            @Override
            public void onFailure(Call<RutasWebDto> call, Throwable t) {
                Log.d("ERROR: ", t.toString());
            }
        });
    }

    private void getCoords(Location location) {
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
        }
    }

    private void checkLocationPermission() {
        //Verifica permisos de locazacion
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                new AlertDialog.Builder(this)
                        .setTitle("Necesitas Permisos de GPS")
                        .setMessage("Esta Aplicacion necesita permisos de gps para la ubicacion actual")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // Solicitar a la usuario una vez que se haya mostrado la explicación
                                ActivityCompat.requestPermissions(MapsActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION );
                            }
                        })
                        .create()
                        .show();
            } else {
                // No se necesita explicación, podemos solicitar el permiso.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION );
            }
        }
    }

    private void AlertNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("El sistema GPS esta desactivado, ¿Desea activarlo?")
                .setCancelable(false)
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        alert = builder.create();
        alert.show();
    }

    class ProgressTask extends AsyncTask<Void, Void, Void>{

        private ProgressDialog pd;
        private CharSequence ruta;
        private Context context;
        private int idRuta;

        public ProgressTask(Context context,CharSequence ruta) {
            pd = new ProgressDialog(context);
            this.context = context;
            this.ruta= ruta;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setTitle("Procesando...");
            pd.setMessage("Dibujando Ruta.");
            pd.setCancelable(false);
            pd.setIndeterminate(true);
            pd.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (pd!=null) {
                pd.dismiss();
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                String selection = ruta.toString();
                idRuta = Integer.parseInt(selection.substring(0, 1));
                if(latitude!=null && longitude!=null)
                    cargarRutas(idRuta);
                else
                    mostrarDialogo();
            } catch (Exception e) {
                Log.e(Constantes.ERROR, e.getMessage(),e);
            }
            return null;
        }

    }



}
